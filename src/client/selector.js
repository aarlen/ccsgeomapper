export function initSelector(maps) {
    const $maps = $(maps);
    var options = []; 

    $maps.each(function() {
        const map = this;
        options.push(`<option>${map.Name}</option>`);
    });
    
    $( "#maps" )
        .append(options.join(""))
        .selectmenu({
            change : function(event,ui) {
                const map = ui.item.label;
                console.log(`${map} selected`);
                $('#map').attr('src',`../media/map/${map}.png`);
            }
        });

}