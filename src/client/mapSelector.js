export function initMapSelector() {
    const $map = $('#map');
    const $tooltip = $('#tooltip');
    const mapSize = {
        w:$map.width(),
        h:$map.height()
    };

    function pxToLonlat(pos) {
        return {
            lon : 90 + ((-pos.y / mapSize.h) * 180),
            lat : -180 + ((pos.x / mapSize.w) * 360)
        }
    }

    function mousePosToLonlat(e) {
        const mousePos = {
            x : e.clientX,
            y : e.clientY
        };
        const elemPos = $map.position();
        const relativePos = {
            x: mousePos.x - elemPos.left,
            y: mousePos.y - elemPos.top
        };
        console.log(`pixel ${e.clientX}x${e.clientY}`);
        const lonlat = pxToLonlat(relativePos);
        console.log(`lonlat ${lonlat.lon}x${lonlat.lat}`);
        return lonlat;
    }

    $map.mousemove(function(event){
        const tPosX = event.pageX + 50;
        const tPosY = event.pageY - 20;
        const lonlat = mousePosToLonlat(event);
        $tooltip.html(`Lon=${lonlat.lon} <br> Lat=${lonlat.lat}`);
        $tooltip.css({'position': 'absolute', 'top': tPosY, 'left': tPosX});
    });

    $map.mouseover(function() {
        $tooltip.removeClass('hidden');
    });

    $map.mouseleave(function() {
        $tooltip.addClass('hidden');
    });



    $map.click(function(e) {

    });
}