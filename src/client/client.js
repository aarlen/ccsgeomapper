import {initSelector} from './selector.js';
import {initMapSelector} from './mapSelector.js';

export function init() {
$.ajax({
    url : '/getConfigurations',
    dataType: "json",
    success : function(result) {
        console.log(result);

        initSelector(result.Maps);
        initMapSelector();
    }
});

}