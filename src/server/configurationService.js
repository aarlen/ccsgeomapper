exports.register = function(server) {
    const registerUri = function(uri,response) {
        server.get(uri, response);
        server.post(uri, response);
        server.head(uri, response); 
    }

    function getConfigurations(req, res, next) {

        var fs = require('fs');
        var obj = JSON.parse(fs.readFileSync('configuration/config.json', 'utf8'));
        res.send(obj);
        next();
      }
    
    registerUri('/getConfigurations', getConfigurations);
    
    console.log('registered tile services');
}