exports.initialize = function(server) {
    const configurationService = require('./configurationService');
    configurationService.register(server);
    console.log('registered all services');
}